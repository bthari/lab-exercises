from collections import namedtuple
from concurrent.futures import ProcessPoolExecutor, as_completed
import multiprocessing as mp
import random
import time

# VALUES = (100, 200, 500, 1000)
# Coin = namedtuple('Coin', ['value'])
nums = [25, 36, 42, 88, 99]

def factorize_naive(n):
    """ A naive factorization method. Take integer 'n', return list of
        factors.
    """
    if n < 2:
        return []
    factors = []
    p = 2

    while True:
        if n == 1:
            return factors

        r = n % p
        if r == 0:
            factors.append(p)
            n = n // p
        elif p * p >= n:
            factors.append(n)
            return factors
        elif p > 2:
            # Advance in steps of 2 over odd numbers
            p += 2
        else:
            # If p == 2, get to 3
            p += 1
    assert False, "unreachable"

def reader(queue):
    termination_threshold = 25
    termination_count = 0
    read = {}

    while termination_count < termination_threshold:
        if queue.empty():
            print("[Process {}] Waiting for new items...".format(
                  mp.current_process().name))
            time.sleep(random.random() * 0.50)
            termination_count += 1
        else:
            termination_count = 0
            num = queue.get()
            factors = (factorize_naive(num))
            read.update({num: factors})
            time.sleep(random.random() * 0.50)
            print("[Process {}] Read coin ({})".format(
                mp.current_process().name, str(num), str(factors)))

    print("[Process {}] Total value read: {}".format(
          mp.current_process().name, str(read)))
    print("[Process {}] Exiting...".format(mp.current_process().name))


def writer(count, queue):

    for ii in range(count):
        num = nums[ii]
        queue.put(nums)

        # No need to prepend string with process name since this
        # function is executed in main interpreter thread
        print("Put coin ({}) into queue".format(num))
        time.sleep(random.random() * 0.50)

if __name__ == '__main__':
    start_time = time.time()
    count = len(nums)
    queue = mp.Queue()  # Queue class from multiprocessing module

    reader_p1 = mp.Process(target=reader, name='Reader 1', args=(queue,))
    reader_p2 = mp.Process(target=reader, name='Reader 2', args=(queue,))
    reader_p1.daemon = True
    reader_p2.daemon = True
    reader_p1.start()
    reader_p2.start()

    writer(count, queue)
    reader_p1.join()
    reader_p2.join()

    end_time = time.time()

    print('Total running time: ' + str(end_time - start_time))
